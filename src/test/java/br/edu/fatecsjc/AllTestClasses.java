package br.edu.fatecsjc;

import org.junit.runner.RunWith;
import org.junit.runner.Runner;
import org.junit.runners.Suite;

@RunWith(Runner.class)
@Suite.SuiteClasses({Exercicio1Test.class, Exercicio2Test.class, Exercicio3Test.class, Exercicio4Test.class, Exercicio5Test.class,
        Exercicio6Test.class, Exercicio7Test.class, Exercicio8Test.class, Exercicio9Test.class, Exercicio10Test.class
})
public class AllTestClasses {


}
