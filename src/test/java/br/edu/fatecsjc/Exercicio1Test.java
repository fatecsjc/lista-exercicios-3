package br.edu.fatecsjc;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class Exercicio1Test {

    private Exercicio1 exercicio1;

    @Before
    public void init() {

        exercicio1 = new Exercicio1();
    }

    @Test
    public void testaValorMaiorOuIgualAZeroEValorMenorOuIgualADez() {

        float valor = exercicio1.verificarValorNotaDigitada(4.0f);
        assertTrue(valor >= 0 && valor <= 10);
    }


}
