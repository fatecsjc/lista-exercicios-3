package br.edu.fatecsjc;

public class Exercicio4 {

    private int n;

    Exercicio4() {
    }

    Exercicio4(int n) {
        this.n = n;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public int calcularFibonnaci(int n) {

        return n == 0 || n == 1 ? n : calcularFibonnaci(n - 1) + calcularFibonnaci(n - 2);
    }
}
