package br.edu.fatecsjc;

import java.util.Scanner;

public class Exercicio1 {

    private float nota;

    public Exercicio1() {
    }

    public Exercicio1(float nota) {
        this.nota = nota;
    }

    public float getNota() {
        return nota;
    }

    public void setNota(float nota) {
        this.nota = nota;
    }

    public float verificarValorNotaDigitada(float nota){

        while (nota < 0 || nota > 10){

            System.out.println("Informe uma nota > 0 e < 10: ");
            Scanner scanner = new Scanner(System.in);
            nota = scanner.nextFloat();
        }

        // System.out.printf("Nota digita: %2.2f", nota);

        return nota;
    }
}
